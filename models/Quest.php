<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $number
 * @property string $description
 * @property int $answer
 * @property int $is_done
 * @property int $is_fail
 *
 */
class Quest extends ActiveRecord
{
    public static function tableName()
    {
        return 'quest';
    }

    public function rules()
    {
        return [
            ['number', 'required'],
            ['number', 'integer'],

            ['description', 'required'],
            ['description', 'string'],

            ['answer', 'required'],
            ['answer', 'string'],

            ['is_done', 'boolean'],

            ['is_fail', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'answer' => 'Ответ',
        ];
    }
}
