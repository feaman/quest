<?php

use app\assets\AppAsset;
use app\models\Quest;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

/* @var $quest Quest */
/* @var $firstUndoneQuest Quest */
/* @var $dummy boolean */
/* @var $errors array */
/* @var $error boolean */
/* @var $_quest Quest */
/* @var $this yii\web\View */

$this->title = 'КВЕСТ!';
?>

<div class="text-center">

    <div>
        <?php foreach (Quest::find()->orderBy(['number' => SORT_ASC])->all() as $_quest) {
            $class = 'primary';
            if ($_quest->id !== $quest->id) {
                $class = $_quest->is_done === 1 ? 'success' : 'default';
            }
            if ($_quest->is_done || $firstUndoneQuest && $_quest->id === $firstUndoneQuest->id) { ?>
                <a class="btn btn-<?= $class ?>" href="/quest/<?= $_quest->id ?>" role="button"><?= $_quest->number ?></a>
            <?php } else { ?>
                <button class="btn btn-<?= $class ?>" role="button" disabled><?= $_quest->number ?></button>
            <?php } ?>
        <?php } ?>
    </div>

    <?php if ($dummy) { ?>
        <h1>Следует пройти предыдущие задания!</h1>
    <?php } else { ?>

        <div>
            <h1>Квест № <?= $quest->number ?></h1>
        </div>

        <div>
            <h4><?= $quest->description ?></h4>
        </div>

        <img class="img-responsive center-block" src="/images/<?= $quest->number ?>.<?= in_array($quest->number, [1]) ? 'gif' : 'jpg' ?>">

        <br>

        <?php if ($error) { ?>
            <p class="bg-danger" style="padding: 15px; font-size: 22px;"><?= $errors[rand(0, count($errors) - 1)] ?></p>
            <?php $this->registerJs("
                $(\"html, body\").animate({ scrollTop: $(document).height() }, \"slow\");
            ", View::POS_END) ?>
        <?php } ?>

        <div>
            <?php if ($quest->is_done) { ?>
                <p class="bg-success" style="padding: 15px;"><?= $quest->answer ?></p>
            <?php } else { ?>
                <div class="container-fluid">
                    <?php
                    $form = ActiveForm::begin([
                        'options' => ['class' => 'form-horizontal'],
                    ]) ?>

                    <div class="form-group">
                        <input type="text" name="answer" class="form-control input-lg" placeholder="Ответ" style="height: 56px; -ms-text-align-last: right;text-align-last: center;">
                    </div>

                    <div class="form-group text-uppercase" style="width: 295px; margin: 0 auto;">
                        <div class="pull-left">
                            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary text-uppercase btn-lg']) ?>
                        </div>
                        <a class="btn btn-default btn-lg" href="/fail/<?= $quest->id ?>" role="button">Сдаюсь...</a>
                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            <?php } ?>
        </div>

    <?php } ?>

</div>
