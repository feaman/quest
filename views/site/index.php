<?php

/* @var $firstUndoneQuestId int */
/* @var $allDone boolean */

/* @var $this yii\web\View */

$this->title = 'КВЕСТ!';
?>

<div class="jumbotron">
    <?php if ($allDone) { ?>
        <h1>Ура, товарищи!</h1>
    <?php } else if ($firstUndoneQuestId) { ?>
        <h1>Квест в разгаре!</h1>
        <p><a class="btn btn-lg btn-success" href="/quest/<?= $firstUndoneQuestId ?>">Продолжить</a></p>
    <?php } else { ?>

        <h2>Марина!</h2>

        <h4>
            Мы знаем, что ты давно борешься с демонами. Особенно во снах. В твоей крови течёт кровь охотницы! И сейчас нам нужна твоя помощь...
            <br><br>
            Твоего мужа похитили тёмные силы! Мы пытались его спасти, но потерпели неудачу.
            <br><br>
            <img class="img-responsive center-block" src="/images/0.jpg">
            <br><br>
            Но у нас есть сведения, которые помогут тебе найти его.
        </h4>
        <br>
        <br>
        <a class="btn btn-lg btn-success" href="/quest/1">Вперёд!</a>
    <?php } ?>
</div>
