<?php

use app\models\Quest;

/* @var $quest Quest */
/* @var $quests array */
/* @var $this yii\web\View */

$this->title = 'КВЕСТ!';
?>

<table class="table text-center">
    <tr>
        <td>Номер</td>
        <td>Пройден ли</td>
        <td>Завален ли</td>
    </tr>
    <?php foreach ($quests as $quest) { ?>
        <tr>
            <td><?= $quest->number ?></td>
            <td>
                <?php if ($quest->is_done) { ?>
                    <span class="label label-success">Да</span>
                <?php } ?>
            </td>
            <td>
                <?php if ($quest->is_done && $quest->is_fail) { ?>
                    <span class="label label-danger">Да</span>
                <?php } else if ($quest->is_done && !$quest->is_fail) { ?>
                    <span class="label label-success">Нет</span>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
</table>

<a class="btn btn-default" href="/status" style="width: 100px; margin: 0 auto; display: block;">Обновить</a>