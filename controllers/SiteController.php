<?php

namespace app\controllers;

use app\models\Quest;
use yii;
use yii\base\UserException;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $questsQuantity = (int)Quest::find()->count();
        $doneQuests = Quest::find()->where(['is_done' => 1])->orderBy(['number' => SORT_DESC])->all();

        $allDone = false;
        $firstUndoneQuestId = null;
        if ($questsQuantity === count($doneQuests)) {
            $allDone = true;
        } else if ($doneQuests) {
            $firstUndoneQuestId = array_shift($doneQuests)->id + 1;
        }

        return $this->render('index', ['firstUndoneQuestId' => $firstUndoneQuestId, 'allDone' => $allDone]);
    }

    public function actionFinish()
    {
        $allQuests = Quest::find()->count();
        $doneQuests = Quest::find()->where(['is_done' => 1])->count();

        if ($allQuests !== $doneQuests) {
            return $this->redirect('/');
        }

        return $this->render('finish');
    }

    public function actionFail($id)
    {
        if (!$quest = Quest::findOne($id)) {
            throw new UserException('Задание ' . $id . ' не найдено');
        }

        $quest->is_done = 1;
        $quest->is_fail = 1;
        $quest->save(false);

        /** @var Quest $nextQuest */
        if ($nextQuest = Quest::find()->where(['is_done' => 0])->orderBy(['number' => SORT_ASC])->one()) {
            return $this->redirect('/quest/' . $nextQuest->id);
        } else {
            return $this->redirect('/finish');
        }
    }

    public function actionQuest($id)
    {
        if (!$quest = Quest::findOne($id)) {
            throw new UserException('Задание ' . $id . ' не найдено');
        }

        $dummy = false;
        /** @var Quest $firstDoneQuest */
        $firstDoneQuest = Quest::find()->where(['is_done' => 0])->orderBy(['number' => SORT_ASC])->one();
        if ($firstDoneQuest && $firstDoneQuest->id !== $quest->id && $quest->is_done === 0) {
            $dummy = true;
        }

        $error = false;
        if ($answer = Yii::$app->request->post('answer')) {
            if (mb_strtolower($quest->answer, 'utf-8') === mb_strtolower($answer, 'utf-8')) {

                $quest->is_done = 1;
                $quest->save(false);

                /** @var Quest $nextQuest */
                if ($nextQuest = Quest::find()->where(['is_done' => 0])->orderBy(['number' => SORT_ASC])->one()) {
                    return $this->redirect('/quest/' . $nextQuest->id);
                } else {
                    return $this->redirect('/finish');
                }
            } else {
                $error = true;
            }
        }

        $firstUndoneQuest = Quest::find()->where(['is_done' => 0])->orderBy(['number' => SORT_ASC])->one();

        return $this->render('quest', [
            'quest' => $quest,
            'error' => $error,
            'dummy' => $dummy,
            'firstUndoneQuest' => $firstUndoneQuest,
            'errors' => [
                'Ты что! Нет конечно!',
                'Как можно было такое подумать?!',
                'Опомнись! Всё ж просто!',
                'Сосредоточься!<br>На кону наши жизни!',
                'Ты в своём уме?<br>Срочно придумай другой вариант!',
            ],
        ]);
    }

    public function actionStatus()
    {
        $quests = Quest::find()->all();

        return $this->render('status', ['quests' => $quests]);
    }
}
